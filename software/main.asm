
                      .setcpu "6502"
                      .include "zeropage.inc"
                      .include "acia.inc"
                      .include "string.inc"
                      .include "macros.inc"

                      .segment "VECTORS"
                      .word   nmi
                      .word   reset
                      .word   irq

                      .bss
                      BUFFER_LENGTH = 80
buffer:               .res BUFFER_LENGTH + 1, 0


                      .code
reset:                jmp main

nmi:                  rti

irq:                  rti

main:                 cld
                      ldx #$ff
                      txs

                      jsr acia_init
                      load_16 ARG0, s_welcome
                      jsr acia_puts

main_loop:            load_16 ARG0, s_prompt
                      jsr acia_puts
                      load_16 ARG0, buffer
                      lda #BUFFER_LENGTH
                      jsr acia_gets
                      lda buffer
                      cmp #'r'
                      bne not_cmd_r
                      jsr cmd_dump
                      jmp main_loop
not_cmd_r:            cmp #'w'
                      bne not_cmd_w
                      jsr cmd_write
                      jmp main_loop
not_cmd_w:            cmp #'j'
                      bne not_cmd_j
                      jsr cmd_jump
                      jmp main_loop
not_cmd_j:            load_16 ARG0, s_unknown_command
                      jsr acia_puts
                      jmp main_loop

cmd_dump:             load_16 ARG0, buffer + 2
                      jsr char_word2int

scan_hex16:           rts
cmd_write:            nop
cmd_jump:             nop


s_welcome:              .byte $0a, $0a, "Custom PC Terminal (v0.1)", $0a, $00
s_unknown_command:      .byte $0a, $0a, "Error: Unknown command", $0a, $00
s_prompt:               .byte "> ", $00
