; Push A and X, destroys A
.macro push_ax
  pha
  txa
  pha
.endmacro

; Push A and Y, destroys A
.macro push_ay
  pha
  tya
  pha
.endmacro

; Push A, X and Y, destroys A
.macro push_axy
  pha
  txa
  pha
  tya
  pha
.endmacro

; Pull A and X
.macro pull_ax
  pla
  tax
  pla
.endmacro

; Pull A and Y
.macro pull_ay
  pla
  tay
  pla
.endmacro

; Pull A, X and Y
.macro pull_axy
  pla
  tay
  pla
  tax
  pla
.endmacro

; Load zero page register reg/reg+1 with the 16-bit value, destroys A
.macro load_16 reg, value
  lda #<(value)
  sta reg
  lda #>(value)
  sta reg + 1
.endmacro
