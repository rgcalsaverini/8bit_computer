.setcpu "6502"
.include "macros.inc"
.include "zeropage.inc"

.export char_byte2int
.export char_word2int

.code

;-----------------------------
; Convert the hex character in the accu to its integer value
; The integer value is returned in the accu
;-----------------------------
char2int:          cmp #'0'
                   bcc invalid
                   cmp #('9' + 1)
                   bcs no_digit
                   sec
                   sbc #'0'
                   rts
no_digit:          cmp #'A'
                   bcc invalid
                   cmp #('F' + 1)
                   bcs no_upper_hex
                   sec
                   sbc #('A' - 10)
                   rts
no_upper_hex:      cmp #'a'
                   bcc invalid
                   cmp #('f' + 1)
                   bcs invalid
                   sec
                   sbc #('a' - 10)
                   rts
invalid:           lda #0
                   rts

;-----------------------------
; Convert a byte at (ARG0) into an integer value
; The integer value is returned in the accu
;-----------------------------
char_byte2int:      tya
                    pha
                    ldy #0
                    lda (ARG0),y
                    jsr char2int
                    asl
                    asl
                    asl
                    asl
                    sta TMP0
                    iny
                    lda (ARG0),Y
                    jsr char2int
                    ora TMP0
                    sta TMP0
                    pla
                    tay
                    lda TMP0
                    rts

;-----------------------------
; Convert four hex characters starting at (ARG0) into an integer value
; The integer value is returned in RES0..RES0+1
;-----------------------------
char_word2int:      push_ay
                    ldy #0
                    lda (ARG0),y
                    jsr char2int
                    asl
                    asl
                    asl
                    asl
                    sta RES0 + 1
                    iny
                    lda (ARG0), Y
                    jsr char2int
                    ora RES0 + 1
                    sta RES0 + 1
                    iny
                    lda (ARG0),y
                    jsr char2int
                    asl
                    asl
                    asl
                    asl
                    sta RES0
                    iny
                    lda (ARG0), Y
                    jsr char2int
                    ora RES0
                    sta RES0
                    pull_ay
                    rts
