.setcpu "6502"

ACIA_DATA = $4000
ACIA_STATUS = $4001
ACIA_COMMAND = $4002
ACIA_CONTROL = $4003

.segment "VECTORS"

.word   nmi
.word   reset
.word   irq

.code

reset:             jmp main

nmi:               rti

irq:               rti

main:
init_acia:        lda #%00001011				;No parity, no echo, no interrupt
                  sta ACIA_COMMAND
                  lda #%00011111				;1 stop bit, 8 data bits, 19200 baud
                  sta ACIA_CONTROL

send:             lda #'R'
                  sta ACIA_DATA
                  nop
                  nop
                  nop
                  nop
                  nop
                  nop
                  nop
                  nop
                  nop
                  nop
loop:             jmp send
