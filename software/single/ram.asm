          .setcpu "6502"
          .segment "VECTORS"

          ; Set vector values
          .word   start
          .word   start
          .word   start

          .code
start:    ldx #21
          nop
          nop
          stx $1000
          nop
          nop
          ldy $1000
          nop
          cpy #21
          beq success
fail:     jmp fail
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
          nop
success:  jmp success


; start:    ldx #$4C
;           stx $0200
;           ldx #$00
;           stx $0201
;           ldx #$02
;           stx $0202
;           ; Jump to SRAM:0200
;           jmp $200
