from random import randint
from py65emu.cpu import CPU
from time import time


def hex_2(num, places=2):
    value = hex(num & (16 ** places - 1))[2:].upper()
    return ('0' * (places - len(value))) + value


class Block(object):
    name = 'Block'

    def __init__(self, start, size, mode):
        self.start = start
        self.size = size
        self.end = self.start + size
        self.can_write = mode in ['RW', 'R/W', 'rw', 'r/w']
        self.reset()

    def __repr__(self):
        return '<{} {} {}-{}>'.format(self.name,
                                      'RW' if self.can_write else 'RO',
                                      hex(self.start)[2:], hex(self.end)[2:])

    def overlaps(self, value):
        if isinstance(value, Block):
            return value.start < self.end < value.end or \
                   self.start < value.end < self.end
        if isinstance(value, list):
            if not value:
                return False
            return any([self.overlaps(b) for b in value])
        if isinstance(value, int):
            return self.start <= value < self.end
        raise TypeError()

    def reset(self):
        raise NotImplementedError()

    def read(self, address):
        index = address - self.start
        return self._read_idx(index)

    def write(self, address, data):
        index = address - self.start
        return self._write_idx(index, data & 0xFF)

    def _read_idx(self, index):
        raise NotImplementedError()

    def _write_idx(self, index, data):
        raise NotImplementedError()

    def draw_lines(self, computer):
        raise NotImplementedError()


class RAM(Block):
    name = 'RAM'

    def __init__(self, start, size):
        self.mem = None
        super(RAM, self).__init__(start, size, 'rw')

    def reset(self):
        self.mem = [randint(0, 0xFF) for _ in range(self.size)]

    def _read_idx(self, index):
        return self.mem[index]

    def _write_idx(self, index, data):
        self.mem[index] = data


class ROM(Block):
    name = 'ROM'

    def __init__(self, start, size, file):
        self.file = file
        self.mem = None
        super(ROM, self).__init__(start, size, 'ro')

    def reset(self):
        self.mem = [0xFF for _ in range(self.size)]
        with open(self.file, 'rb') as fp:
            data = fp.read()
            if len(data) > self.size:
                raise IndexError('ROM too big')
            for i, ch in enumerate(data):
                self.mem[i] = ch

    def _read_idx(self, index):
        return self.mem[index]

    def _write_idx(self, index, data):
        raise TypeError('Read only block')


class ACIA(Block):
    name = 'ACIA'
    baud_map = [None, 50, 74, 109.92, 134.58, 150, 300, 600, 1200, 1800, 2400,
                3600, 4800, 7200, 9600, 19200]

    def __init__(self, start, size):
        self.stop_bits = None
        self.word_len = None
        self.internal_clock = None
        self.baud_rate = None
        self.parity = None
        self.echo = None
        self.transmit_interrupt = None
        self.rts_high = None
        self.irq_enabled = None
        self.dtr_enabled = None
        self.cmd_value = None
        self.ctr_value = None

        self.txd_empty = None
        self.rxd_full = None

        self.data = None
        super(ACIA, self).__init__(start, size, 'rw')

    def reset(self):
        self.set_control(0)
        self.set_command(2)
        self.txd_empty = True
        self.rxd_full = False

    def _write_idx(self, index, data):
        actual_idx = index & 0x3

        if actual_idx == 0x3:
            self.set_control(data)
        elif actual_idx == 0x2:
            self.set_command(data)
        elif actual_idx == 0x1:
            raise NotImplementedError()
        else:
            self.send_byte(data)

    def _read_idx(self, index):
        actual_idx = index & 0x3

        if actual_idx == 0x3:
            return self.ctr_value
        elif actual_idx == 0x2:
            return self.cmd_value
        elif actual_idx == 0x1:
            return self.get_status()
        else:
            raise NotImplementedError()

    def set_control(self, data):
        self.stop_bits = [1, 2][(data & 0x80) >> 7]
        self.word_len = [8, 7, 6, 5][(data & 0x60) >> 5]
        self.internal_clock = [False, True][(data & 0x10) >> 4]
        self.baud_rate = self.baud_map[data & 0xF]
        self.ctr_value = data

    def set_command(self, data):
        self.parity = [False, 'even', 'odd', 'mark', 'spc'][(data & 0xE0) >> 5]
        self.echo = [False, True][(data & 0x10) >> 4]

        transmitter_ctl = (data & 0xC) >> 2
        self.transmit_interrupt = [False, True, False, False][transmitter_ctl]
        self.rts_high = [True, False, False, False][transmitter_ctl]

        self.irq_enabled = [True, False][(data & 0x2) >> 1]
        self.dtr_enabled = [False, True][data & 0x1]
        self.cmd_value = data

    def get_status(self):
        return (self.txd_empty << 4) | (self.rxd_full << 3)

    def send_byte(self, data):
        self.txd_empty = False
        self.data = bytes(bytearray([data & 0xFF]))

    def external_read(self):
        self.txd_empty = True
        return self.data


class MemSpace(object):
    def __init__(self, blocks=None):
        self.blocks = list()
        self.readWord = self.read_word
        for block in blocks or []:
            self.add_block(block)

    def reset(self):
        for block in self.blocks:
            block.reset()

    def find_block(self, address):
        for block in self.blocks:
            if block.overlaps(address):
                return block
        raise IndexError('Unmapped address')

    def add_block(self, block):
        if block.overlaps(self.blocks):
            raise RuntimeError('Overlap')
        self.blocks.append(block)

    def read(self, address):
        block = self.find_block(address)
        return block.read(address)

    def read_word(self, address):
        return (self.read(address + 1) << 8) | self.read(address)

    def write(self, address, data):
        block = self.find_block(address)
        if not block.can_write:
            raise TypeError('Read only block')
        return block.write(address, data)


class Hardware(object):
    def __init__(self, mem: MemSpace):
        start = mem.read_word(0xFFFC)
        self.cpu = CPU(mmu=mem, pc=start)
        self.breakpoints = list()
        self.mem_space = mem
        self.verbose = False

    def step(self):
        pc = self.cpu.r.pc
        if self.verbose:
            print('{} ({}): {}'.format(hex_2(pc, 4)[2:],
                                       self.mem_space.find_block(pc).name,
                                       hex_2(self.mem_space.read(pc))))
        self.cpu.step()
        if self.cpu.r.pc in self.breakpoints:
            raise StopIteration()

    def run(self, max_secs=None):
        start = time()
        try:
            while max_secs is None or time() - start < max_secs:
                self.step()
        except KeyboardInterrupt:
            return False
        except StopIteration:
            self.raise_bp()
            return True
        return False

    def raise_bp(self):
        if self.verbose:
            print('BP @ %s' % hex_2(self.cpu.r.pc))
