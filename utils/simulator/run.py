from sim.computer import MemSpace, RAM, ROM, ACIA, Hardware

ram = RAM(0, 0x4000)
acia = ACIA(0x4000, 0x4000)
rom = ROM(0x8000, 0x8000, './../../software/acia_1.bin')
mem_space = MemSpace([ram, acia, rom])
system = Hardware(mem_space)


def test_acia_1():
    bp_send = 0x801D
    bp_delay = 0x8024

    system.breakpoints.append(bp_send)
    system.breakpoints.append(bp_delay)

    msg = b''
    expected = b'Hello World!\n'

    while True:
        on_bp = system.run(1)
        if not on_bp or system.cpu.r.pc == bp_delay:
            break
        assert acia.txd_empty is True
        assert acia.get_status() & 0x10 > 0
        system.step()
        assert acia.txd_empty is False
        assert acia.get_status() & 0x10 == 0
        msg += acia.external_read()

    assert msg == expected, '{} != {}'.format(msg, expected)
    print('test_acia_1 OK')
