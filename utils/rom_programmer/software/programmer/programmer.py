import serial
import time
from serial.serialutil import to_bytes, SerialException
import sys

MAX_ADDR = 0xFFFF
STATUS_VALID = 0x21

CMD_HANDSHAKE = 0x10
CMD_SELECT = 0x1A
CMD_IDENTIFY = 0x20
CMD_FILL = 0x30
CMD_CHECK = 0x40
CMD_WRITE = 0x50
CMD_READ = 0x60
CMD_ERASE = 0x70

TARGET_SST = 0x00
TARGET_AT = 0x01
TARGET_UNKNOWN = 0xFF


def hex2(val):
    hex_val = hex(val)[2:]
    return '{}{}'.format('0' if len(hex_val) == 1 else '', hex_val)


class BaseMsgType(object):
    def __init__(self, value):
        self.value = value
        self.encoded = self._encode(value)

    def _encode(self, value):
        raise NotImplementedError()

    def append_to(self, payload: bytes):
        payload += self.encoded


class Word(BaseMsgType):
    def _encode(self, value):
        return [(value & 0xFF00) >> 8, value & 0x00FF]


class Byte(BaseMsgType):
    def _encode(self, value):
        return [value & 0x00FF]


def wait_for_data(serial_port, num_bytes, timeout=0):
    start = time.time()
    while serial_port.in_waiting < num_bytes:
        if time.time() - start > timeout > 0:
            raise TimeoutError()
        time.sleep(0.001)
    return serial_port.read(num_bytes)


class Response(object):
    def __init__(self, status, message, size):
        self.status = status
        self.error = status != STATUS_VALID
        self.bytes = to_bytes(message or [])
        self.list = [int(c) for c in self.bytes]
        self.size = size

    def __getitem__(self, item):
        return self.list[item]


def send_msg(serial_port: serial.Serial, command: int, message=None,
             debug=False):
    payload = Byte(command).encoded
    for part in message or []:
        payload.extend(part.encoded)

    if debug:
        sys.stderr.write('> ' + ' '.join(hex2(v) for v in payload) + '\n')
        sys.stderr.flush()

    serial_port.write(to_bytes(payload))
    length, status_code = wait_for_data(serial_port, 2)

    if length > 0:
        msg = wait_for_data(serial_port, length)
    else:
        msg = None

    if debug:
        sys.stderr.write(' < ({}) {}: {}\n\n'.format(length, hex2(status_code),
                                                     ' '.join(hex2(v) for v in
                                                              (msg or []))))
        sys.stderr.flush()

    return Response(status_code, msg, length)


class DryProgrammer(object):
    def fill(self, addr_start: int, addr_end: int, data, page: int):
        print('Filling {}-{}:{} = {}'.format(addr_start, addr_end, page, data))

    def erase(self):
        print('Erasing chip')

    def check(self, addr_start: int, addr_end: int, data, page: int):
        print('Check {}-{}:{} = {}'.format(addr_start, addr_end, page, data))

    def unknown_state(self):
        print('Set unknown state')


class Programmer(object):
    __magic_str = [0xBA, 0xBA, 0xCA]
    __allowed_versions = [0x01]

    def __init__(self, target, device_path='/dev/ttyACM1', force=False,
                 debug=False):
        self.device_path = device_path
        self._serial = None
        self.prog_version = None
        self.force = force is True
        self.debug = debug is True
        self.target = target

    @property
    def serial_port(self):
        if self._serial is None:
            try:
                self._serial = serial.Serial(self.device_path, 38400)
                time.sleep(3)
            except SerialException:
                raise ConnectionError(
                    'Could not open serial port at "%s"' % self.device_path)
            if not self._valid_handshake(self._serial):
                raise AssertionError('Invalid handshake')
            self._select_target(self._serial, self.target)
            if self.target == TARGET_SST and not self._check_device(
                    self._serial):
                raise AssertionError('Unknown device')
        return self._serial

    def send_msg(self, command: int, message=None):
        return send_msg(self.serial_port, command, message, self.debug)

    def _valid_handshake(self, serial_port: serial.Serial):
        for i in range(6):
            res = send_msg(serial_port, CMD_HANDSHAKE)
            if i > 0:
                time.sleep(0.1)
            if res.error:
                continue
            if res.size != len(self.__magic_str) + 1:
                continue
            if not all([res[i] == self.__magic_str[i] for i in
                        range(len(self.__magic_str))]):
                continue
            if res[-1] not in self.__allowed_versions:
                continue
            self.prog_version = res[-1]
            return True
        return False

    def _check_device(self, serial_port: serial.Serial):
        res = send_msg(serial_port, CMD_IDENTIFY, debug=self.debug)
        return self.force or res.size == 2 and res[0] == 0xBF and res[
            1] == 0xB5

    def _select_target(self, serial_port: serial.Serial, target):
        res = send_msg(serial_port, CMD_SELECT, [Byte(target)],
                       debug=self.debug)
        if res.error:
            raise KeyError('Device selection failed')

    def unknown_state(self):
        try:
            self._select_target(self.serial_port, TARGET_UNKNOWN)
        except KeyError:
            pass

    def erase(self):
        res = self.send_msg(CMD_ERASE)
        if res.error:
            raise RuntimeError('Could not erase chip')

    def fill(self, addr_start: int, inpt_addr_end: int, data, page: int):
        assert page in [0, 1]
        assert 0 <= addr_start <= MAX_ADDR
        assert 0 <= inpt_addr_end <= MAX_ADDR + 1
        success = False

        if inpt_addr_end > MAX_ADDR:
            self.write(0xFFFF, data, page)
            addr_end = 0xFFFF
        else:
            addr_end = inpt_addr_end

        for i in range(3):
            res = self.send_msg(CMD_FILL,
                                [Word(addr_start), Word(addr_end), Byte(data),
                                 Byte(page)])
            if not res.error:
                success = True
                break
            time.sleep(0.05)

        if not success:
            raise RuntimeError('Failed to write byte')

    def check(self, addr_start: int, inpt_addr_end: int, data, page: int):
        assert page in [0, 1]
        assert 0 <= addr_start <= MAX_ADDR
        assert 0 <= inpt_addr_end <= MAX_ADDR + 1

        if inpt_addr_end > MAX_ADDR:
            if self.read(0xFFFF, page) != data:
                return False
            addr_end = 0xFFFF
        else:
            addr_end = inpt_addr_end

        res = self.send_msg(CMD_CHECK,
                            [Word(addr_start), Word(addr_end), Byte(data),
                             Byte(page)])
        if res.error:
            return False
        return True

    def write(self, addr, data, page=None):
        if self.target == TARGET_AT:
            self.send_msg(CMD_WRITE, [Word(addr), Byte(data)])
            res = self.send_msg(CMD_READ, [Word(addr)])

            if not res.error and res[0] == data:
                return True
        elif self.target == TARGET_SST:
            res = self.send_msg(CMD_WRITE,
                                [Word(addr), Byte(data), Byte(page)])

            if not res.error:
                return True
        return False

    def read(self, addr, page=None):
        if self.target == TARGET_SST:
            res = self.send_msg(CMD_READ, [Word(addr), Byte(page)])
            if not res.error:
                return res[0]
        if self.target == TARGET_AT:
            res = self.send_msg(CMD_READ, [Word(addr)])
            if not res.error:
                return res[0]

        return None
