import yaml
from .programmer import Programmer


class Range(object):
    __clock_map = {
        1: 0,
        2: 1,
        4: 2,
        8: 3,
        16: 4,
    }

    def __init__(self, addr_from, addr_to, mode, block_id, clock_div):
        assert clock_div in [1, 2, 4, 8, 16]
        assert mode.lower() in ['ro', 'rw']
        assert 0 <= block_id <= 15
        assert addr_from < addr_to

        self.addr_from = addr_from
        self.addr_to = addr_to
        self.value = block_id | (self.__clock_map[clock_div] << 4)
        self.pages = [1] if mode.lower() == 'ro' else [0, 1]

    def program(self, prog):
        for page in self.pages:
            length = self.addr_to - self.addr_from
            print(' - {} as {} on page {}...'.format(length, self.value, page))
            prog.fill(self.addr_from, self.addr_to, self.value, page=page)

    def check(self, prog):
        for page in self.pages:
            print(' - Checking block with id {}...'.format(self.value))
            res = prog.check(self.addr_from, self.addr_to, self.value, page)
            if res is False:
                raise RuntimeError('Block is wrong')


class Mapper(object):
    def __init__(self, path):
        self.ranges = list()

        with open(path, 'r') as stream:
            mem_map = yaml.safe_load(stream)['blocks']

        for entry in mem_map:
            self.ranges.append(Range(
                addr_from=entry['start'],
                addr_to=entry['end'],
                clock_div=entry.get('clock_div', 1),
                mode=entry.get('mode', 'rw'),
                block_id=entry['id']
            ))

    def program(self, programmer: Programmer):
        programmer.erase()

        for block in self.ranges:
            block.program(programmer)

        for block in self.ranges:
            block.check(programmer)
