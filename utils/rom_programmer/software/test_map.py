from programmer import Programmer
from programmer.programmer import TARGET_SST, TARGET_AT
import sys

prog = Programmer(TARGET_SST, sys.argv[1], debug=True)
prog.erase()
for i in range(0xFF):
    prog.write(i, i & 0xFF, 0)
for i in range(0x0100, 0x10000, 0x1000):
    prog.write(i, i & 0xFF, 0)
