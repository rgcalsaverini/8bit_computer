"""Check Flash memory health

Usage:
  health_check.py [--fixed=<sp>] [--random=<num>]

Options:
  -h --help          Show this screen.
  -f --fixed=<sp>    Fixed spacing (hex) [default: 400].
  -r --random=<num>  Num of random points [default: 200]

"""
from docopt import docopt

from programmer.programmer import Programmer
import random
import time

arguments = docopt(__doc__)


def hex2(val, length):
    hex_val = hex(val)[2:]
    return '{}{}'.format('0' * (length - len(hex_val)), hex_val.upper())


def get_check_points():
    space = int(arguments['--fixed'], 16)
    num_random = int(arguments['--random'])
    points = [0, 1, 2, 3, 4, 5, 0xFFFF, 0xFFFE, 0xFFFD, 0xFFFC]
    points.extend([p for p in range(space, 0xFFFF, space)])
    points.extend([random.randint(0, 0xFFFF) for _ in range(num_random)])
    random.shuffle(list(set(points)))
    return {p: random.randint(2, 254) for p in points}


def write_all(mem_map, programmer):
    progress = 0.0
    per_byte = 100.0 / float(len(mem_map.keys()))
    last_reported = None
    for addr, val in mem_map.items():
        if last_reported is None or progress - last_reported > 1:
            last_reported = progress
            print('%d%%' % int(progress))
        programmer.write(addr, val, 0)
        time.sleep(0.01)
        programmer.write(addr, val, 1)
        time.sleep(0.01)
        progress += per_byte


def check(mem_map, programmer):
    failures = list()
    progress = 0.0
    per_byte = 100.0 / float(len(mem_map.keys()))
    last_reported = None
    for addr, expected_val in mem_map.items():
        if last_reported is None or progress - last_reported > 1:
            last_reported = progress
            print('%d%%' % int(progress))
        for page in [0, 1]:
            read_val = programmer.read(addr, page)
            if expected_val != read_val:
                failures.append({'addr': addr, 'expected': expected_val, 'read': read_val, 'page': page})
        progress += per_byte
    return failures


def cover_map(fails, points, page):
    output = ('PAGE %d\n\n' % page)
    coverage = [[' ' for _ in range(128)] for __ in range(32)]
    fail_addr = [p['addr'] // 16 for p in fails if p['page'] == page]

    for pt in points.keys():
        conv_addr = pt // 16
        line = conv_addr // 128
        col = conv_addr % 128
        coverage[line][col] = '╳' if conv_addr in fail_addr else '▇'

    output += '╭%s╮\n' % ('─' * 128)
    for line in coverage:
        output += '│%s│\n' % ''.join(line)
    output += '╰%s╯\n\n' % ('─' * 128)

    output += '\n'
    return output


def write_report(points, fails):
    output = cover_map(fails, points, 0) + cover_map(fails, points, 1)
    with open('report.out', 'w') as fp:
        fp.write(output)


if __name__ == "__main__":
    prog = Programmer()
    mem_pts = get_check_points()
    print('Writing %d bytes...' % len(mem_pts.keys()))
    write_all(mem_pts, prog)
    print('Checking values on memory...')
    mem_fails = check(mem_pts, prog)
    pct_fails = int(50.0 * float(len(mem_fails)) / float(len(mem_pts.keys())))

    print('Finished. {} ({}%) of the test failed.'.format(len(mem_fails), pct_fails))
    print("Read detailed report on 'report.out'")
    write_report(mem_pts, mem_fails)
