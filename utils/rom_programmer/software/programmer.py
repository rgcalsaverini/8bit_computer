"""Usage:
  programmer.py sst FILE [--force] [--debug] [--path=PATH][--dry]
  programmer.py sst FILE --bin [--force] [--debug] [--path=PATH]  [--dry] [--start=START]
  programmer.py at FILE [--debug]
  programmer.py dump_at FROM TO OUT  [--debug]
  programmer.py fill_at FROM TO VALUE [--debug]


Arguments:
  FILE        yaml memory mapping file
  FROM
  TO
  OUT

Options:
  -b --bin              Write binary file
  -x --dry              Dry mode
  -p --path=<PATH>      TTY path
  -s --start=<START>    Start
  -h --help
  -f --force            Force write, even with wrong chip ID
  --debug               Debug mode
"""

from docopt import docopt
from programmer import Mapper, Programmer, DryProgrammer
from programmer.programmer import TARGET_SST, TARGET_AT


def program_bin(programmer, path, start_addr):
    with open(path, 'rb') as fp:
        file_data = fp.read()

    print('Contents: {} KB ({} bytes of data)'.format(
        len(file_data), len([v for v in file_data if v != 0xFF])))

    if programmer.target == TARGET_SST:
        programmer.erase()
    for addr, bt in enumerate(file_data):
        if programmer.target == TARGET_SST and bt == 0xFF:
            continue
        if bt % 0x800 == 0:
            print('{}%'.format(int(100 * float(addr) / len(file_data))))
        programmer.write(addr + start_addr, bt, page=0)
        val = programmer.read(addr + start_addr, page=0)
        if val is None or val != bt:
            raise ValueError('Write failed')


def dump(programmer, addr_s, addr_e, path):
    data = list()
    for addr in range(addr_s, addr_e):
        bt_value = programmer.read(addr)
        if bt_value is None:
            raise RuntimeError()
        data.append(bt_value)

    with open(path, 'wb') as fp:
        fp.write(bytes(bytearray(data)))


if __name__ == '__main__':
    arguments = docopt(__doc__)
    from_arg = int(arguments['FROM'], 16) if arguments['FROM'] else None
    to_arg = int(arguments['TO'], 16) if arguments['TO'] else None
    value_arg = int(arguments['VALUE'], 16) if arguments['VALUE'] else None

    if arguments['sst']:
        if arguments['--dry']:
            prog = DryProgrammer()
        else:
            prog = Programmer(TARGET_SST, arguments['--path'],
                              force=arguments['--force'],
                              debug=arguments['--debug'])

        try:
            if arguments['--bin']:
                arg_start = arguments['--start']
                start_addr = int(arg_start, 16) if arg_start else 0
                program_bin(prog, arguments['FILE'], start_addr)
            else:
                mem_map = Mapper(arguments['FILE'])
                mem_map.program(prog)
        finally:
            prog.unknown_state()
    elif arguments['dump_at']:
        prog = Programmer(TARGET_AT, debug=arguments['--debug'])
        dump(prog, from_arg, to_arg, arguments['OUT'])
    elif arguments['fill_at']:
        prog = Programmer(TARGET_AT, debug=arguments['--debug'])
        for addr in range(from_arg, to_arg):
            prog.write(addr, value_arg)

    else:
        raise NotImplementedError()
