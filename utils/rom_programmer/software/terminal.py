import serial
from serial.serialutil import to_bytes
import sys
import select
import tty
import termios
from termcolor import colored
import sys

baud = sys.argv[2] if len(sys.argv) > 2 else 38400
ser = serial.Serial(sys.argv[1], baud)

old_settings = termios.tcgetattr(sys.stdin)


def is_data():
    return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])


def get_data(inpt):
    if len(inpt) % 2 == 1:
        return None
    msg = [v for v in inpt if v.upper() in "0123456789ABCDEF"]
    nums = list()
    for i in range(0, len(msg), 2):
        nums.append(int(msg[i] + msg[i + 1], 16))
    return to_bytes(nums)


dt_buffer = ''

try:
    tty.setcbreak(sys.stdin.fileno())
    while True:
        if ser.in_waiting:
            data = ser.read(ser.in_waiting)
            print(colored(' '.join(hex(v) for v in data), 'blue'))
            sys.stdout.flush()

        if is_data():
            c = sys.stdin.read(1)
            if c in ['\n', '\x0a']:
                data = get_data(dt_buffer.upper().strip())
                if data:
                    ser.write(data)
                    print('\n>', colored(dt_buffer, 'red'), '\n')
                    sys.stdout.flush()
                else:
                    dt_buffer = ''
                    print()
                    sys.stdout.flush()
                dt_buffer = ''
            elif c in ['\b', '\x08']:
                dt_buffer = ''
                print()
                sys.stdout.flush()
            else:
                dt_buffer += c
                print(c, end='')
                sys.stdout.flush()
except KeyboardInterrupt:
    pass
finally:
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)
