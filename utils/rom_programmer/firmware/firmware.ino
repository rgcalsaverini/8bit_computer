#import "flash.h"
#import "serial.h"

#define CMD_HANDSHAKE 0x10
#define CMD_SELECT    0x1A   // 0x1A device[1]
#define CMD_IDENTIFY  0x20   
#define CMD_FILL      0x30   // 0x30 from[2] to[2] value[1] page[1]
#define CMD_CHECK     0x40   // 0x40 from[2] to[2] value[1] page[1]
#define CMD_WRITE     0x50   // 0x50 addr[2] value[1] page[1]
#define CMD_READ      0x60   // 0x60 addr[2] page[1]
#define CMD_ERASE     0x70

/* Programmer commands */
void handshake();
void selectTarget();
void identifyDevice();
void fillRange();
void checkRange();
void writeByte();
void readByte();
void eraseChip();

void setup() {
  flash_setup();
  Serial.begin(38400);
}

void loop() {
  byte command;
  if(ser_readByte(&command)) {
    if(selectedTargetDevice == FLASH_TARGET_SST) {
      switch (command) {
        case CMD_HANDSHAKE: handshake();      break;
        case CMD_SELECT:    selectTarget();   break;
        case CMD_IDENTIFY:  identifyDevice(); break;
        case CMD_FILL:      fillRange();      break;
        case CMD_CHECK:     checkRange();     break;
        case CMD_WRITE:     writeByte();      break;
        case CMD_READ:      readByte();       break;
        case CMD_ERASE:     eraseChip();      break;
      }
    } else if(selectedTargetDevice == FLASH_TARGET_AT) {
      switch (command) {
        case CMD_HANDSHAKE: handshake();      break;
        case CMD_SELECT:    selectTarget();   break;
        case CMD_WRITE:     writeByte();      break;
        case CMD_READ:      readByte();       break;
      }
    } else {
      byte unselected_error[] = {0x66, command, selectedTargetDevice};
      switch (command) {
        case CMD_HANDSHAKE: handshake();      break;
        case CMD_SELECT:    selectTarget();   break;
        default: ser_error(unselected_error, 3); break;
      }
    }
  }
}

void handshake() {
  byte hs_message[] = {0xBA, 0xBA, 0xCA, 0x01};
  ser_response(hs_message, 4);
}

void selectTarget() {
  byte target_id;

  if(!ser_readByte(&target_id)) return;
  bool res = flash_selectTarget(target_id);
  byte msg[] = {target_id};
  if (!res) {
    ser_error(msg, 1);
    return;
  }
  ser_response(msg, 1);
}

void identifyDevice() {
  sst_setPage(0);
  word id = sst_getIdentification();
  byte message[] {id >> 8, id & 0xFF};
  ser_response(message, 2);
}

void fillRange() {
  word from;
  word to;
  byte value;
  byte page;
  
  if(!ser_readWord(&from)) return;
  if(!ser_readWord(&to)) return;
  if(!ser_readByte(&value)) return;
  if(!ser_readByte(&page)) return;

  sst_setPage(page);
    
  for (word addr = from; addr < to ; addr++) {
    sst_writeByte(addr, value);
  }
  ser_response(NULL, 0);
}

void checkRange() {
  word from;
  word to;
  byte value;
  byte page;
  
  if(!ser_readWord(&from)) return;
  if(!ser_readWord(&to)) return;
  if(!ser_readByte(&value)) return;
  if(!ser_readByte(&page)) return;

  sst_setPage(page);

  for(word addr = from ; addr < to ; addr++) {
    if (value != flash_readByte(addr)) {
      byte message[] = {addr >> 8, addr & 0xFF};
      ser_error(message, 2);
      return;
    }
  }
  ser_response(NULL, 0);
}

void writeByte() {
  word addr;
  byte value;
  byte page;

  if(!ser_readWord(&addr)) return;
  if(!ser_readByte(&value)) return;

  if (selectedTargetDevice == FLASH_TARGET_SST) {
    if(!ser_readByte(&page)) return;
  
    sst_setPage(page);
    sst_writeByte(addr, value);
    
    if (sst_readSameAddr() != value) {
      ser_error(NULL, 0);
      return;
    }
  } else if(selectedTargetDevice == FLASH_TARGET_AT) {
    at_writeByte(addr, value);
  }
  ser_response(NULL, 0);
}

void readByte() {
  word addr;
  byte page;
  
  if(!ser_readWord(&addr)) return;
  
  if (selectedTargetDevice == FLASH_TARGET_SST) {
    if(!ser_readByte(&page)) return;
    sst_setPage(page);
  }

  byte data = flash_readByte(addr);
  byte message[] = {data};
  ser_response(message, 1);
}

void eraseChip() {
  sst_writeByte(0x0000, 0x0);
  sst_eraseChip();
  
  if (flash_readByte(0x0000) != 0xFF) {
    ser_error(NULL, 0);
    return;
  }
  ser_response(NULL, 0);
}
