/*
  [1] http://www.ti.com/lit/ds/symlink/sn74hc595.pdf
  [2] http://ww1.microchip.com/downloads/en/DeviceDoc/20005022C.pdf
*/
#define FLASH_TARGET_SST    0x00
#define FLASH_TARGET_AT     0x01
#define FLASH_TARGET_X      0xFF

#define SCS_PROGRAM         0xA0
#define SCS_ERASE_FIRST     0x80
#define SCS_ERASE_CHIP      0x10
#define SCS_ID_ENTRY        0x90
#define SCS_ID_EXIT         0xF0

#define ADDR_MANUFACTURER 0x0
#define ADDR_DEVICE 0x1

#define addrClockPin    10
#define addrDataPin     11
#define addrLatchPin    12
#define outputEnablePin A1
#define WESSTPin        A2
#define pagePin         A3
#define VddATEnablePin  A4
#define A14orWEATPin    A5

const int dataBusPins[] = {2, 3, 4, 5, 6, 7, 8, 9};
byte currentDataBusMode = INPUT;
int selectedTargetDevice = FLASH_TARGET_SST;

/* Top level functions */
void flash_setup();
bool flash_selectTarget(byte target);
byte flash_readByte(word address);

/* AT flash instructions */
void at_writeByte(word addr, byte value);

/* SST flash instructions */
word sst_getIdentification();
byte sst_readSameAddr();
void sst_writeByte(word addr, byte value);
void sst_eraseChip();
void sst_setPage(byte page);
void sst_command(byte code);

/* Basic bus IO */
void flash_writeCycle(word address, byte data);
void flash_setWE(bool value);
void flash_setAddressBus(word address);
void flash_setDataBus(byte data);
byte flash_getDataBus();
void flash_dataBusMode(byte mode);


void flash_setup() {
  flash_dataBusMode(INPUT);
  pinMode(addrClockPin, OUTPUT);
  pinMode(addrDataPin, OUTPUT);
  pinMode(addrLatchPin, OUTPUT);
  pinMode(outputEnablePin, OUTPUT);
  pinMode(WESSTPin, INPUT); // safe initial state for unknown device
  pinMode(pagePin, OUTPUT);
  pinMode(VddATEnablePin, OUTPUT);
  pinMode(A14orWEATPin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  digitalWrite(outputEnablePin, HIGH);
  digitalWrite(WESSTPin, HIGH);
  digitalWrite(pagePin, LOW);
  digitalWrite(A14orWEATPin, HIGH); // avoid inadvertent write on AT
  digitalWrite(VddATEnablePin, HIGH); // disable vdd2 on start
  digitalWrite(LED_BUILTIN, HIGH);
}

bool flash_selectTarget(byte target) {
  if (target == FLASH_TARGET_SST) {
    pinMode(WESSTPin, OUTPUT);
    digitalWrite(VddATEnablePin, HIGH);
    selectedTargetDevice = FLASH_TARGET_SST;
    digitalWrite(LED_BUILTIN, selectedTargetDevice == FLASH_TARGET_X);
  } else if (target == FLASH_TARGET_AT){
    digitalWrite(VddATEnablePin, LOW);
    digitalWrite(A14orWEATPin, HIGH);
    pinMode(WESSTPin, INPUT);
    selectedTargetDevice = FLASH_TARGET_AT;
    digitalWrite(LED_BUILTIN, selectedTargetDevice == FLASH_TARGET_X);
  } else {
    // safe "unknown device" state
    digitalWrite(A14orWEATPin, HIGH);
    digitalWrite(VddATEnablePin, HIGH);
    pinMode(WESSTPin, INPUT);
    selectedTargetDevice = FLASH_TARGET_X;
    digitalWrite(LED_BUILTIN, selectedTargetDevice == FLASH_TARGET_X);
    return false;
  }
  return true;
}

word sst_getIdentification() {
  /*
     Following figure 18 on [2] pg. 19
     Ida ([2] pg. 13) asks for at least 150ns before reading.
  */
  word id = 0x0000;
  sst_command(SCS_ID_ENTRY);
  delayMicroseconds(1); // Wait

  id = flash_readByte(ADDR_MANUFACTURER) << 8;
  id |= flash_readByte(ADDR_DEVICE);

  sst_command(SCS_ID_EXIT);
  delayMicroseconds(1); // Wait Ida (pg. 13) at least 150ns
  return id;
}

byte flash_readByte(word address) {
  /* Read one byte from address
     [2] pg.13 puts Toe (OE pulse time) at 30ns
     Tolz as 0ns (can immediately read after OE low)
     and Taa as 70 (address access time) at 70ns
  */
  flash_dataBusMode(INPUT); // first of all
  flash_setWE(HIGH);
  flash_setAddressBus(address);
  digitalWrite(outputEnablePin, LOW);
  byte data = flash_getDataBus();
  digitalWrite(outputEnablePin, HIGH);
  return data;
}

byte sst_readSameAddr() {
  flash_dataBusMode(INPUT);
  digitalWrite(WESSTPin, HIGH);
  digitalWrite(outputEnablePin, LOW);
  byte data = flash_getDataBus();
  digitalWrite(outputEnablePin, HIGH);
  return data;
}

void sst_writeByte(word address, byte data) {
  // [2] figure 16 on pg. 21
  sst_command(SCS_PROGRAM);
  flash_writeCycle(address, data);
  delayMicroseconds(30); // Wait Tbp ([2]pg. 13) at least 20us
}

void at_writeByte(word address, byte data) {
  flash_writeCycle(address, data);
  delay(15);
}

void sst_eraseChip() {
  // Following figure 19 on pg. 22
  sst_command(SCS_ERASE_FIRST);
  sst_command(SCS_ERASE_CHIP);
  delay(200); // Wait Tsce ([2] pg. 13) at least 100ms
}

void sst_setPage(byte page) {
  digitalWrite(pagePin, page);
}

void sst_command(byte code) {
  /* Perform a software command sequence */
  flash_writeCycle(0x5555, 0xAA);
  flash_writeCycle(0x2AAA, 0x55);
  flash_writeCycle(0x5555, code);
}

void flash_writeCycle(word address, byte data) {
  /* Do a simple write cycle to the IC
    [2] pg.13 says the address should be held for 30ns and
    that WE pulse should be at least 30ns. Again no need to delay
  */
  digitalWrite(outputEnablePin, HIGH);
  flash_setWE(HIGH);
  flash_setAddressBus(address);
  flash_setDataBus(data);
  delayMicroseconds(5); // just paranoia, should not be necessary
  flash_setWE(LOW);
  delayMicroseconds(5); // paranoia again
  flash_setWE(HIGH);
  delayMicroseconds(5); // paranoia again
}

void flash_setDataBus(byte data) {
  /* Puts an 8-bit value on the data bus.
     [2] pg.13 asks for at least 30ns for data setup time,
     so again no need to delay.
     This is another speed bottleneck.
  */
  flash_dataBusMode(OUTPUT);
  for (short i = 0 ; i < 8 ; i++) {
    digitalWrite(dataBusPins[i], bitRead(data, i));
  }
}

byte flash_getDataBus() {
  flash_dataBusMode(INPUT);
  byte data;
  for (short i = 0 ; i < 8 ; i++) {
    bitWrite(data, i, digitalRead(dataBusPins[i]));
  }
  return data;
}

void flash_setWE(bool value) {
  if(selectedTargetDevice == FLASH_TARGET_SST) {
    digitalWrite(WESSTPin, value);
  } else if(selectedTargetDevice == FLASH_TARGET_AT){
    digitalWrite(A14orWEATPin, value); // WE on AT 
  }
}

void flash_setAddressBus(word address) {
  /* Shifts out a 16-bit address to the address bus.
     According to [1] pg.7, clock should not exceed 25MHz
     but Uno's shiftOut clock was measured to only 100KHz.
     This is probably the speed bottleneck.
  */
  digitalWrite(addrLatchPin, LOW);
  shiftOut(addrDataPin, addrClockPin, MSBFIRST, address >> 8);
  shiftOut(addrDataPin, addrClockPin, MSBFIRST, address & 0xFF);
  // If target is SST, this is A14
  if(selectedTargetDevice == FLASH_TARGET_SST) {
    digitalWrite(A14orWEATPin, (address & 0x4000) >> 14); //A14 
  }
  digitalWrite(addrLatchPin, HIGH);
}

void flash_dataBusMode(byte mode) {
  if (currentDataBusMode == mode) {
    return;
  }
  for (short i = 0 ; i < 8 ; i++) {
    pinMode(dataBusPins[i], mode == OUTPUT ? OUTPUT : INPUT);
  }
  currentDataBusMode = mode;
}
