#define SER_TIMEOUT_MS 100

#define SER_RESP_VALID 0x21
#define SER_RESP_ERROR 0x00

byte ser_readCommand();
word ser_readWord();
byte ser_readByte();
bool ser_waitForData(int num_bytes);
void ser_response(byte *message, size_t lenght);
void ser_error(byte *message, size_t lenght);
void ser_sendMessage(byte *message, byte status_code, size_t lenght);

byte ser_readByte(byte *data) {
  if (!ser_waitForData(1)) {
    return false;  
  }
  *data = Serial.read();  
  return true;
}

bool ser_readWord(word *data) {
  if (!ser_waitForData(2)) {
    return false;  
  }
  byte ch1 = Serial.read();
  byte ch2 = Serial.read();
  *data = (ch1 << 8) | ch2;
  return true;
}

bool ser_waitForData(int num_bytes) {
  long start = millis();
  
  while(Serial.available() < num_bytes) {
    if(millis() - start > SER_TIMEOUT_MS) {
      return false;
    }
  }
  return true;
}

void ser_response(byte *message, size_t lenght) {
  ser_sendMessage(message, SER_RESP_VALID, lenght);
}

void ser_error(byte *message, size_t lenght) {
  ser_sendMessage(message, SER_RESP_ERROR, lenght);
}

void ser_sendMessage(byte *message, byte status_code, size_t lenght) {
  byte full_message[lenght + 2];
  memcpy(full_message + 2, message, lenght);
  full_message[0] = lenght;
  full_message[1] = status_code;
  Serial.write(full_message, lenght + 2);
}
