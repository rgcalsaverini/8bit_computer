# 8-Bit Computer

This is an attempt at building an 8-Bit computer from scratch based on the MOS 6502 CPU (the same processor
one on the lovely [Nintendo Entertainment System](https://en.wikipedia.org/wiki/Nintendo_Entertainment_System)). 
It will be a full computer, with:
- ROM and RAM
- keyboard input
- Serial communication over USB
- video and sound output
- a (true) random number generator
- SD card input
- configurable timer interrupts
- I2C bus
- an expansion slot (for old-school-cartridge-like stuff) 

This is frustratingly hard, so it stays abandoned for long periods of time.

This is how the last debug board (but not the actual production design) looks like:

![](./last_board.png)

## Architecture and design

Coming soon. This README is a draft

## What is included here

- **Case**: Nothing yet, but will be the CAD drawings of the case
- **docs**: Mostly data sheets and the development journal
- **hardware**: Hardware CAD files for the boards
- **software**: 6502 assembly and ROM
- **utils** Some utilities. Most notably a ROM programmer used to flash software the map of the address decoder