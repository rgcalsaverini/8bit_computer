# 1. Sep 2020 #

One year later and I don't understand a lot of what was going
on...

Maybe I should have documented more :)

Still, I'm sure I can resume and try to tackle the Serial problem,
that seems to be the next big challenge.

I am convinced that using the FT245 is the way to go.

I would first try to interface the FT245 to arduino to understand
better how it works. Than I would have to finish the serial
interface circuit. As it is, it seems to be RO and even worse,
it looks like it is shorting the data bus.

Maybe I could just use the 74AC245 instead of 74AC157.

----------------------------------------

# 13. Jun #

Acia module pins are switched.

EN -> IRQ
IRQ -> RES
RES -> R/W
RW -> RS0
RS0 -> RS1
RS1 -> PHI2
PHI2 -> EN


----------------------------------------

# 4. Jun #

6551 not working. Code seems to be okay.

Good references:
  - http://forum.6502.org/viewtopic.php?f=4&t=3347
  - http://forum.6502.org/viewtopic.php?f=12&t=5111
  - https://www.avrfreaks.net/forum/aciauart-6551-current-equivalent
  -

Should try a few things:
  - Short TxD, RxD on FT232 and try to echo stuff
    - Yup, I get echo.
  - Review code and debug with oscilloscope
    - I get no clock on RxC. Shouldn't I?
  - Reassemble 6551 on another protoboard
    - Same thing.
  - Interface 6551 with arduino [like this](https://github.com/gaku/fun-arduino/blob/master/acia6551/acia6551.ino)
  - Exactly replicate Grappendorf's PC

----------------------------------------

# 3. Jun #

CPU module working perfectly, including RAM. Some improvements and fixes were
necessary.

Waiting for time to assemble and test ACIA.

- ~~Re-wire and double check RAM~~
  - RAM working on module. Don't know what the issue was, but it is resolved.
- ~~Assemble and use dev modules~~
  - CPU module assembled an working.
- **Assemble and test ACIA**
- **Develop ACIA bootloader to facilitate software development**
- ~~Add address stretching circuit~~
  - Postponed, necessity is questionable.


----------------------------------------

# 1. Jun #

Assemble the CPU dev module. Some points:
- Change value of C2 to 100nf
- Change C1 package to a tighter one
- Labels on resistors are confusing
- Some vias are way to close do pads, increase via-pad clearance
- Clock jumper too close to ZIF lever
- RAM package is wrong, too wide. Pick a tighter one
- Capacitor C1 was reversed (fixed on schematic)
- Pin 1 of CPU unconnected.
- Missing labels on jumpers
- On jumpers, label which pin goes to IC
- !READ and !WRITE are swapped


Test of the CPU module:
- [x] VCC within specs **(NO - 3V only)**
  - _Replaced with another supply_
- [x] Reset.
- [x] Clock signal
- [x] Pinout on 6502
- [x] Pinout on ROM
- [x] Pinout on RAM
- [x] !READ and !WRITE **(NO - Swapped [ref](http://wilsonminesco.com/6502primer/ClkGen.html))**
  - _Jumped it_
- [X] NOP (EA)
- [x] BRK (00)
- [X] JMP (4C)

Okay... After patching the !READ and !WRITE signals with jumper cables,
it seems to work.  



----------------------------------------

# 28. Mai #

The Address decoder might add uneeded complexity while I test the ACIA.
So, I will use the following basic glue logic for now:

<img src="./glue.svg" alt="drawing" width="350"/>

Created a basic program to test it out.

----------------------------------------

# 27. Mai #

Finished and ordered the CPU and ACIA PCBs.

Got the RAM ICs.

Since it is JEDEC compatible, it could be used on the
programmer. I was able to write and read it successfully.

I made the program `ram.asm` to test the RAM with the CPU.
After running, if the RAM usage was successful, it would stay on address range
0x8028-0x8030, if it was not, it would stay on range 0x8011-0x8013.

The test consisted in running it without the RAM IC, and then again with it.
Since the pinout was the same, I changed nothing.

It failed, as both attempts ended up on the _fail_ address range. it could be:
- An error with the program
  - Unlikely, very simple and worked on the emulator.
- Faulty memory
  - Unlikely, I am able to use it on the programmer
- Sync error
  - The memory is very fast. Could be the breadboard setup, but also unlikely.
- Wiring error
  - Very much possible...
- Something else
  - Everything is possible with this project. Who knows.


  - ~~Replace RAM IC with a proper fast RAM~~
  - ~~Design a simple TTL IO Board or rush serial comm with ACIA~~
  - ~~Assemble Dev PCB~~
  - **Re-wire and double check RAM**
  - **Assemble and use dev modules**
  - Add address stretching circuit


----------------------------------------

# 24. Mai #

Okay, so programs are working, almost completely sure by analyzing the signals.
Buuuut... RAM is not.

I did the following test: i would write 0 to addresss $0180, 2 to $0280 and
so on until 5 to address $0680.

The code varied slightly, such as if it repeated of stopped after one execution,
and if the code was padded with NOPs between each instruction  

No repeat, lots of NOPs between instructions:

```
00000000  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000c40  40 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |@...............|
00000c50  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00001f80  3f 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |?...............|
00001f90  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 ff  |................|
00001fa0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00002000
```

Repeat, no nops:
```
00000000  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000040  05 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000050  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000240  05 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000250  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000440  06 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000450  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000005c0  01 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000005d0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000640  01 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000650  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00002000
```

So... **Something** is getting written on RAM. The addresses that are being written
to are wrong, but they are addresses that at some point were put into the address bus,
such as PC. The data is also not noise, it is the same values that it should write.

Now, according to the [datasheet](https://www.mouser.de/datasheet/2/268/doc0270-1108115.pdf)
(S. 14 pg 8) and the [forum](http://forum.6502.org/viewtopic.php?f=12&t=4389)
my IC choice for RAM should be okay. But still not conclusive, and this
test seems to indicate some synching issues.

Now, Grappendorf uses [a proper SRAM](http://pdf.datasheetcatalog.com/datasheets2/19/190062_1.pdf) with very little delay (< 20ns).
Perhaps I should replace my IC by that and try again.

- ~~Investigate data bus wiring across whole circuit~~
- ~~Add RAM, test write.~~
  - ~~Re-validate basic program.~~
- **Replace RAM IC with a proper fast RAM**
- **Design a simple TTL IO Board or rush serial comm with ACIA**
- Assemble Dev PCB
  - **Update Dev PCB, see if it is usable as is (RAM replacement and changes)**
- Add address stretching circuit


----------------------------------------

# 22. Mai #

Ordered dev board PCB.

Hooked up RAM. Doesn't seem to be working. ~~Might be something wrong with the
programmer, as every few KB seems to be mirrored on terminal.py.~~ (the AT only has
8K, of course it was mirrored).
Also, the program address was not consistent. Might be a code error...

I must remember to use !READ on ROM, otherwise writing to it will lead to
output collision.

- Investigate data bus wiring across whole circuit
- Add RAM, test write.
  - **Re-validate basic program.**
- Assemble Dev PCB
- Add address stretching circuit

----------------------------------------

# 21. Mai #
So, validating the address decoder:

**1. Correctly decodes mem map to select lines**

Did not at first. Ended up being a wiring error on the data bus of the programmer... Again.
Now I'm not sure if the SST's data bus is wired correctly anywhere else. Should debug
that on the main board as well.

After fixing that it decodes properly.

**2. Decodes address in a reasonable timeframe**

Measured a bit, and it seems that the propagation delay is pretty consistent and
around 45ns.
As per the datasheets of both ICs involved, a maximum of 70ns could be expected.
At 2.5 MHz the cycle lasts 400ns, which means that decoding would take 11-20% of the
cycle time. On 1 MHz should be 5-8%.
All in all, I think it is okay for the clock range I plan to work with.
It is the equivalent of 3 or 4 levels of TTL logic in propagation time.

|                |    |    |    |    |    |    |    | Average | Std Dev |
|----------------|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:-------:|:-------:|
| Rising Bottom  | 20 | 19 | 22 | 22 | 21 | 19 | 22 |    21   |   1.38  |
| Rising Top     | 40 | 44 | 41 | 40 | 42 | 39 | 42 |    41   |   1.68  |
| Falling Top    | 42 | 42 | 42 | 42 | 40 | 41 | 40 |    41   |   0.95  |
| Falling Bottom | 41 | 41 | 41 | 41 | 41 | 40 | 40 |    41   |   0.49  |

The measurement represents the delay in ns between the clock signal,
that was driving an address line on the memory, and one output on the decoder.


**3. Other questions**

- The 75159's low level seems pretty high (V<sub>OL</sub>), datasheet seems to
contradict that. Might want to investigate further.

Roadmap:
  - ~~Validate Address decoder~~
  - **Investigate data bus wiring across whole circuit**
  - Add RAM, test write.
  - Assemble Dev PCB
  - Add address stretching circuit

----------------------------------------

# 20. Mai #

Decided to use the remaining bits from the address decoder memory to control the
clock stretching. Using some kind of counter, it should be able to provide granular
stretching. Pretty cool heh :)

**Danger** The counter would not be synchronized to the very first bit, so
the first stretched pulse after switching sources should have an unpredictable length.
Might be able to fix this by clearing and preseting to 0b000 or 0b111 on every switch.
That might be slow and need extra ICs tho.

Stopped everything and started the dev board. It should make life easier.

Still have to:
  - Validate decoder design (timing, correctness)
  - Test and validate RAM
  - Buy parts

Roadmap:
  - Validate Address decoder
  - Add RAM, test write.
  - Assemble Dev PCB
  - Add address stretching circuit

  ----------------------------------------

# 18. Mai #

The IO board is not working. But by looking at the signals, the programs
definitively are.

It is probably a timing issue, there is a 3.8 µs response delay from the PIC,
and given that Φ2 period is 1 µs, that is waaaay to much. I don't think that
i could get away with more than ~200ns.

<img src="./io_timing.svg" alt="drawing" width="350"/>

I could try a few things:
- Ditch MCC and reduce complexity on ISR functions, that was nasty anyway
  - Down to 1.8 µs. Still awful
- Program the PIC in assembler
  - Too much work for such a low expectation of success.
- Get a faster (way faster) MCU
  - Look it up.
- Do some clock stretching
  - Might be the most robust solution. But by no means easy
- Don't use a PIC at all, and make a faster board with logic ICs
  - Can I really avoid MCUs alltogether? What about I2C, SD...
- Use registers as buffers and an extra one for ready flags
  - Lots and lots of extra ICs, complex hardware and adds software complications
- Check whole path, migh be TTL delay

----------------------------------------

# 17. Mai #

I've finished the IO board, should be enough for now.
Next I should wire it up, get some minimal address decoding working and trying
to write programs again.

The project is growing too big and complex for my breadboarding skills... I might
benefit from creating a small 6502 dev board to continue.

Also, assembled the programmer's PCB. Lessons learned:

- The 5mm resistor is too small... Should pick the 7.5mm
- Size 10 lines seem to be good enough.
- Spacing 16 seem to be good enough.
- Prepare better before soldering, separate material, properly support PCB...

----------------------------------------

# 16. Mai #
Got the assembler working, but the programs are not running correctly.
~~Might be a wiring problem on the address bus.~~ I was not paying attention to
the actual addressing on the CPU. Double checked and stepped the memory, the
connections are okay.

Tried this minimal ROM:

```
0000800 aaa9 004c 00e8 0000 0000 0000 0000 0000
...
0001ff0 0000 0000 0000 0000 0000 e800 e800 e800
```

It _kinda_ looks like it might be working. But I have no means to know it for sure...
The next priority will be adding some PIC-based IO device and finishing
the address decoding.

----------------------------------------

# 15. Mai #
Changes:
- I had swapped two wires on the programmer. The PCB also seems to be wrong, but fixable in firmware.
- Changed the ROM IC from AT to SST. Cheaper and faster.

Programmed the whole ROM with 4C. Since the opcode 4CXXXX jumps to XXXX, on reset it would
start executing on address 4C4C, and keep jumping to the same address.
I would expect the address to go, in order 4C4C, 4C4D, 4C4E and repeat. It was consistent with
what I saw on the oscilloscope:

![](./jmp_addr.png)

The remaining address bits were also sampled and correct.

 Also tried filling the ROM with NOPs, and it keept sweeping across the whole address space.
 When filled with BRKs, it went FFFE, FFFF, 0000 and repeat.

 Seems like it is working so far.

- !! Double check wiring on Address Decoder's SST. Data seems wrong
